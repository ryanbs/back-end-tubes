<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;       //import model User
use Validator;      //import library untuk validasi

class AuthController extends Controller
{
    public function register(Request $request){
        $registrationData = $request->all();
        $validate = Validator::make($registrationData, [
            'name' => 'required|max:50|regex:/^[a-zA-Z0-9 ]*$/',
            'username' => 'required|max:50|alpha_num|unique:users',
            'password' => 'required|min:6|max:16',
            'email' => 'required|email:rfc,dns|unique:users',
            'notelp' => 'required|max:20',
            'alamat' => 'required|max:120'
            
        ]); //membuat rule validasi input
        
        if($validate->fails())
            return response(['message' => $validate->errors()],400); //return error invalid input
        
        $registrationData['password'] = bcrypt($request->password); //enkripsi password
        $user = User::create($registrationData); //membuat user baru
        return response([
            'message' => 'Register Success',
            'user' => $user,
        ],200); //return data user dalam bentuk json
    }

    public function login(Request $request){
        $loginData = $request->all();
        $validate = Validator::make($loginData, [
            'username' => 'required|max:50|alpha_num',
            'password' => 'required|min:6|max:16'
        ]); //membuat rule validasi input

        if($validate->fails())
            return response(['message' => $validate->errors()],400); //return error invalid input

        if(!Auth::attempt($loginData))
            return response(['message' => 'Invalid Credentials'],401); //return error gagal login

        $user = Auth::user();
        $token = $user->createToken('Authentication Token')->accessToken; //generate token

        return response([
            'message' => 'Authenticated',
            'user' => $user,
            'token_type' => 'Bearer',
            'access_token' => $token
        ]); //return data user dan token dalam bentuk json
    }

    public function logout (Request $request) 
    {
        $token = $request->user()->token();
        $token->revoke();
        return response()->json([
            'message' => 'Successfully logged out',
        ],200);
    
    }

    public function show($id){
        $user = User::find($id); //mencari data product berdasarkan id

        if(!is_null($user)){
            return response([
                'message' => 'Retrieve Item Success',
                'data' => $user
            ],200);
        } //return data product yang ditemukan dalam bentuk json

        return response([
            'message' => 'User Not Found',
            'data' => null
        ],404); //return message saat data product tidak ditemukan
    }

    public function index(){
        $users = User::all(); //mengambil semua data product
        
        if(count($users) > 0){
            return response([
                'message' => 'Retrieve All Success',
                'data' => $users
            ],200);
        } //return data semua product dalam bentuk json

        return response([
            'message' => 'Empty',
            'data' => null
        ],404); //return message data product kosong
    } 

    public function updateUser(Request $request, $id){
        $user = User::find($id); //mencari data product berdasarkan id
        if(is_null($user)){
            return response([
                'message' => 'User Not Found',
                'data' => null
            ],404); 
        } //return message saat data product tidak ditemukan

        $updateData = $request->all(); //mengambil semua input dari api client
        $validate = Validator::make($updateData, [
            'name' => 'max:50|regex:/^[a-zA-Z0-9 ]*$/',
            'notelp' => 'max:20',
            'alamat' => 'max:120'
        ]); //membuat rule validasi input

        if($validate->fails())
            return response(['message' => $validate->errors()],400); //return error invalid input
        
        $user->name = $updateData['name']; //edit nama_produk
        $user->notelp = $updateData['notelp']; //edit stok
        $user->alamat = $updateData['alamat']; //edit stok

        if($user->save()){
            return response([
                'message' => 'Update User Success',
                'data' => $user,
            ],200);
        } //return data product yang telah di edit dalam bentuk json
        return response([
            'message' => 'Update User Failed',
            'data' => null,
        ],400); //return message saat product gagal di edit
    } 
}
