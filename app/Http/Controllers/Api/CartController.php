<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;  //import library untuk validasi
use App\Cart; //import model Cart
use DB;

class CartController extends Controller
{
     //method untuk menampilkan semua data product (read)
     public function index(){
        $carts = Cart::all(); //mengambil semua data product
        
        if(count($carts) > 0){
            return response([
                'message' => 'Retrieve All Success',
                'data' => $carts
            ],200);
        } //return data semua product dalam bentuk json

        return response([
            'message' => 'Empty',
            'data' => null
        ],404); //return message data product kosong
    } 

    //method untuk menampilkan 1 data product (search)
    public function show($id){
        $cart = 
            DB::table('carts')
            ->join('items','carts.id_item','=','items.id')
            ->select('carts.id','items.nama_produk','items.harga','carts.jumlah'
            ,'items.id as item_id',
            DB::raw('items.harga * carts.jumlah as total'))
            ->where('carts.id_user',$id)->get(); //mencari data product berdasarkan id

        if(!is_null($cart)){
            return response([
                'message' => 'Retrieve Cart Success',
                'data' => $cart
            ],200);
        } //return data product yang ditemukan dalam bentuk json

        return response([
            'message' => 'Cart Not Found',
            'data' => null
        ],404); //return message saat data product tidak ditemukan
    }

    //method untuk menambah 1 data product baru (create)
    public function store(Request $request){
        $storeData = $request->all(); //mengambil semua input dari api client
        $validate = Validator::make($storeData, [
            'id_user' => 'required|numeric',
            'id_item' => 'required|numeric',
            'jumlah' => 'required|numeric'
        ]); //membuat rule validasi input
        
        if($validate->fails())
            return response(['message' => $validate->errors()],400); //return error invalid input
        
        $cart = Cart::where('id_user', $storeData['id_user'])->where('id_item', $storeData['id_item'])->first();
        if(!$cart){
            $cart = Cart::create($storeData);
            return response([
                'message' => 'Add to Cart Success',
                'data' => $cart,
            ],200);
        }else{
            return response([
                'message' => 'Add to Cart',
                'data' => null,
            ],200);
        }
    } 

    //method untuk menghapus 1 data product (delete)
    public function destroy($id){
        $cart = Cart::find($id); //mencari data product berdasarkan id
        
        if(is_null($cart)){
            return response([
                'message' => 'Cart Not Found',
                'data' => null
            ],404);
        } //return message saat data product tidak ditemukan

        if($cart->delete()){
            return response([
                'message' => 'Delete Cart Success',
                'data' => $cart,
            ],200);
        } //return message saat berhasil menghapus data product
        return response([
            'message' => 'Delete Cart Failed',
            'data' => null,
        ],400); //return message saat gagal menghapus data product
    }

    //method untuk mengubah 1 data product (update)
    public function update(Request $request, $id){
        $cart = Cart::find($id); //mencari data product berdasarkan id
        if(is_null($cart)){
            return response([
                'message' => 'Cart Not Found',
                'data' => null
            ],404); 
        } //return message saat data product tidak ditemukan

        $updateData = $request->all(); //mengambil semua input dari api client
        $validate = Validator::make($updateData, [
            'jumlah' => 'numeric'
        ]); //membuat rule validasi input

        if($validate->fails())
            return response(['message' => $validate->errors()],400); //return error invalid input
        
        $cart->jumlah = $updateData['jumlah']; //edit stok

        if($cart->save()){
            return response([
                'message' => 'Update Cart Success',
                'data' => $cart,
            ],200);
        } //return data product yang telah di edit dalam bentuk json
        return response([
            'message' => 'Update Cart Failed',
            'data' => null,
        ],400); //return message saat product gagal di edit
    } 
    public function total_cart($id){
        $total = DB::table('carts')
        ->join('items', 'carts.id_item', '=', 'items.id')
        ->select(DB::raw('sum(carts.jumlah * items.harga) AS total_barang'))
        ->where('carts.id_user',$id)->first();
        return response([
            'message' => 'Total Barang',
            'data' => $total,
        ],200);
    }
    public function beli($id){
        $beli = DB::table('carts')->where('id_user',$id)->delete();
        return response([
            'message' => 'Sukses beli barang',
            'data' => null,
        ],200);
    }
}
