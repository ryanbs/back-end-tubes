<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;  //import library untuk validasi
use App\Item; //import model Item

class ItemController extends Controller
{
    //method untuk menampilkan semua data product (read)
    public function index(){
        $items = Item::all(); //mengambil semua data product
        
        if(count($items) > 0){
            return response([
                'message' => 'Retrieve All Success',
                'data' => $items
            ],200);
        } //return data semua product dalam bentuk json

        return response([
            'message' => 'Empty',
            'data' => null
        ],404); //return message data product kosong
    } 

    //method untuk menampilkan 1 data product (search)
    public function show($id){
        $item = Item::find($id); //mencari data product berdasarkan id

        if(!is_null($item)){
            return response([
                'message' => 'Retrieve Item Success',
                'data' => $item
            ],200);
        } //return data product yang ditemukan dalam bentuk json

        return response([
            'message' => 'Item Not Found',
            'data' => null
        ],404); //return message saat data product tidak ditemukan
    }

    //method untuk menambah 1 data product baru (create)
    public function store(Request $request){
        $storeData = $request->all(); //mengambil semua input dari api client
        $validate = Validator::make($storeData, [
            'nama_produk' => 'required|max:50|regex:/^[a-zA-Z0-9 ]*$/',
            'harga' => 'required|numeric|gt:0',
            'stok' => 'required|alpha'
        ]); //membuat rule validasi input
        
        if($validate->fails())
            return response(['message' => $validate->errors()],400); //return error invalid input
        
        $item = Item::create($storeData); //menambah data product baru
        return response([
            'message' => 'Add Item Success',
            'data' => $item,
        ],200); //return data product baru dalam bentuk json
    } 

    //method untuk menghapus 1 data product (delete)
    public function destroy($id){
        $item = Item::find($id); //mencari data product berdasarkan id
        
        if(is_null($item)){
            return response([
                'message' => 'Item Not Found',
                'data' => null
            ],404);
        } //return message saat data product tidak ditemukan

        if($item->delete()){
            return response([
                'message' => 'Delete Item Success',
                'data' => $item,
            ],200);
        } //return message saat berhasil menghapus data product
        return response([
            'message' => 'Delete Item Failed',
            'data' => null,
        ],400); //return message saat gagal menghapus data product
    }

    //method untuk mengubah 1 data product (update)
    public function update(Request $request, $id){
        $item = Item::find($id); //mencari data product berdasarkan id
        if(is_null($item)){
            return response([
                'message' => 'Item Not Found',
                'data' => null
            ],404); 
        } //return message saat data product tidak ditemukan

        $updateData = $request->all(); //mengambil semua input dari api client
        $validate = Validator::make($updateData, [
            'nama_produk' => 'max:50|regex:/^[a-zA-Z0-9 ]*$/',
            'harga' => 'numeric|gt:0',
            'stok' => 'alpha'
        ]); //membuat rule validasi input

        if($validate->fails())
            return response(['message' => $validate->errors()],400); //return error invalid input
        
        $item->nama_produk = $updateData['nama_produk']; //edit nama_produk
        $item->harga = $updateData['harga']; //edit harga_jual
        $item->stok = $updateData['stok']; //edit stok

        if($item->save()){
            return response([
                'message' => 'Update Item Success',
                'data' => $item,
            ],200);
        } //return data product yang telah di edit dalam bentuk json
        return response([
            'message' => 'Update Item Failed',
            'data' => null,
        ],400); //return message saat product gagal di edit
    } 
}
