<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;  //import library untuk validasi
use App\Wishlist; //import model Item
use DB;

class WishlistController extends Controller
{
    //method untuk menampilkan semua data product (read)
    public function index(){
        $wishlists = Wishlist::all(); //mengambil semua data product
        
        if(count($wishlists) > 0){
            return response([
                'message' => 'Retrieve All Success',
                'data' => $wishlists
            ],200);
        } //return data semua product dalam bentuk json

        return response([
            'message' => 'Empty',
            'data' => null
        ],404); //return message data product kosong
    } 

    //method untuk menampilkan 1 data product (search)
    public function show($id){
        $wishlist = 
        DB::table('wishlists')
            ->join('items','wishlists.id_item','=','items.id')
            ->select('wishlists.id','items.nama_produk','wishlists.note_produk'
            ,'items.id as item_id')
            ->where('wishlists.id_user',$id)->get(); //mencari data product berdasarkan id //mencari data product berdasarkan id

        if(!is_null($wishlist)){
            return response([
                'message' => 'Retrieve Wishlist Success',
                'data' => $wishlist
            ],200);
        } //return data product yang ditemukan dalam bentuk json

        return response([
            'message' => 'Wishlist Not Found',
            'data' => null
        ],404); //return message saat data product tidak ditemukan
    }

    //method untuk menambah 1 data product baru (create)
    public function store(Request $request){
        $storeData = $request->all(); //mengambil semua input dari api client
        $validate = Validator::make($storeData, [
            'id_user' => 'required|numeric',
            'id_item' => 'required|numeric',
            'note_produk' => 'required|max:200'
        ]); //membuat rule validasi input
        
        if($validate->fails())
            return response(['message' => $validate->errors()],400); //return error invalid input
        
        $wishlist = Wishlist::where('id_user', $storeData['id_user'])->where('id_item', $storeData['id_item'])->first(); //menambah data product baru
        if(!$wishlist){
            $wishlist = Wishlist::create($storeData);
            return response([
                'message' => 'Add to Wishlist Success',
                'data' => $wishlist,
            ],200);
        }else{
            return response([
                'message' => 'Add to Wishlist',
                'data' => null,
            ],200);
        }
    } 

    //method untuk menghapus 1 data product (delete)
    public function destroy($id){
        $wishlist = Wishlist::find($id); //mencari data product berdasarkan id
        
        if(is_null($wishlist)){
            return response([
                'message' => 'Wishlist Not Found',
                'data' => null
            ],404);
        } //return message saat data product tidak ditemukan

        if($wishlist->delete()){
            return response([
                'message' => 'Delete Wishlist Success',
                'data' => $wishlist,
            ],200);
        } //return message saat berhasil menghapus data product
        return response([
            'message' => 'Delete Wishlist Failed',
            'data' => null,
        ],400); //return message saat gagal menghapus data product
    }

    //method untuk mengubah 1 data product (update)
    public function update(Request $request, $id){
        $wishlist = Wishlist::find($id); //mencari data product berdasarkan id
        if(is_null($wishlist)){
            return response([
                'message' => 'Wishlist Not Found',
                'data' => null
            ],404); 
        } //return message saat data product tidak ditemukan

        $updateData = $request->all(); //mengambil semua input dari api client
        $validate = Validator::make($updateData, [
            'note_produk' => 'max:200'
        ]); //membuat rule validasi input

        if($validate->fails())
            return response(['message' => $validate->errors()],400); //return error invalid input
        
        $wishlist->note_produk = $updateData['note_produk']; //edit stok

        if($wishlist->save()){
            return response([
                'message' => 'Update Wishlist Success',
                'data' => $wishlist,
            ],200);
        } //return data product yang telah di edit dalam bentuk json
        return response([
            'message' => 'Update Wishlist Failed',
            'data' => null,
        ],400); //return message saat product gagal di edit
    } 
}
