<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'Api\AuthController@register');
Route::post('login', 'Api\AuthController@login');

Route::group(['middleware' => 'auth:api'], function(){
    Route::get('product', 'Api\ItemController@index');
    Route::get('product/{id}', 'Api\ItemController@show');
    Route::post('product', 'Api\ItemController@store');
    Route::put('product/{id}', 'Api\ItemController@update');
    Route::delete('product/{id}', 'Api\ItemController@destroy');

    Route::get('cart', 'Api\CartController@index');
    Route::get('cart/{id}', 'Api\CartController@show');
    Route::post('cart', 'Api\CartController@store');
    Route::put('cart/{id}', 'Api\CartController@update');
    Route::delete('cart/{id}', 'Api\CartController@destroy');
    Route::get('cartTotal/{id}', 'Api\CartController@total_cart');
    Route::get('cartBeli/{id}', 'Api\CartController@beli');

    Route::get('wishlist', 'Api\WishlistController@index');
    Route::get('wishlist/{id}', 'Api\WishlistController@show');
    Route::post('wishlist', 'Api\WishlistController@store');
    Route::put('wishlist/{id}', 'Api\WishlistController@update');
    Route::delete('wishlist/{id}', 'Api\WishlistController@destroy');

    Route::get('user/{id}', 'Api\AuthController@show');
    Route::put('user/{id}','Api\AuthController@updateUser');
});

